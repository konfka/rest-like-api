package modules

import (
	"gitlab.com/konfka/rest-like-api/auth/internal/infrastructure/component"
	acontroller "gitlab.com/konfka/rest-like-api/auth/internal/modules/auth/controller"
	ucontroller "gitlab.com/konfka/rest-like-api/auth/internal/modules/user/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
