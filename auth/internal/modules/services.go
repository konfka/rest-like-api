package modules

import (
	"gitlab.com/konfka/rest-like-api/auth/internal/infrastructure/component"
	aservice "gitlab.com/konfka/rest-like-api/auth/internal/modules/auth/service"
	"gitlab.com/konfka/rest-like-api/auth/internal/storages"
)

type Services struct {
	//User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		Auth: aservice.NewAuth(storages.Verify, components),
	}
}
