package modules

import (
	"gitlab.com/konfka/rest-like-api/user/internal/infrastructure/component"
	//aservice "gitlab.com/konfka/rest-like-api/user/internal/modules/auth/service"
	uservice "gitlab.com/konfka/rest-like-api/user/internal/modules/user/service"
	"gitlab.com/konfka/rest-like-api/user/internal/storages"
)

type Services struct {
	User uservice.Userer
	//Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		//Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
